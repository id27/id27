============
ID27 project
============

[![build status](https://gitlab.esrf.fr/ID27/id27/badges/master/build.svg)](http://ID27.gitlab-pages.esrf.fr/ID27)
[![coverage report](https://gitlab.esrf.fr/ID27/id27/badges/master/coverage.svg)](http://ID27.gitlab-pages.esrf.fr/id27/htmlcov)

ID27 bliss software

Latest documentation from master can be found [here](http://ID27.gitlab-pages.esrf.fr/id27)
