import os
import gevent
import argparse
import click
import h5py
from bliss.config import static
from bliss.common import session
from bliss.common.scans.ct import ct
import numpy as np
from matplotlib import pyplot as plt


cfg = static.get_config()



def main(args=None):
    laser_heating = cfg.get('laser_heating')
    return _main(args,laser_heating=laser_heating)

def main_lab(args=None):
    laser_heating = cfg.get('laser_heating_lab')
    return _main(args,laser_heating_lab=laser_heating)

def _main(args=None,laser_heating=None):
    default_session = session.DefaultSession()
    session_env = dict()
    default_session.setup(session_env)
    
    arguments = parse_args(args)
    exposure_time = arguments.exposure_time
    temperature = arguments.temperature
    epsilon = arguments.epsilon
    calibration_name = arguments.calibration_name
    camera = laser_heating.camera
    proxy = camera.proxy
    previous_roi = proxy.image_roi
    print('previous_roi',previous_roi)
    proxy.image_roi = (0,0,0,0)
    camera.image.roi = proxy.image_roi
    gevent.sleep(1)
    print('image roi',camera.image.roi)
    try:
        while True:
            #take the dark
            #camera.shutter.mode = "MANUAL"
            proxy.shutter_mode = "MANUAL"
            s = ct(exposure_time,camera.image)
            raw_dark = s.get_data()['image'].get_image(0)
            print('raw_dark shape',raw_dark.shape)
            #take data
            #camera.shutter.mode = "AUTO_FRAME"
            proxy.shutter_mode = "AUTO_FRAME"
            s = ct(exposure_time,camera.image)
            raw_data = s.get_data()['image'].get_image(0)
            print('raw_data shape',raw_data.shape)

            clip_dark = raw_dark[previous_roi[0]:previous_roi[0]+previous_roi[2]]
            clip_data = raw_data[previous_roi[0]:previous_roi[0]+previous_roi[2]]
            data = np.array(clip_data).astype(float).mean(axis=0)
            m =  np.array(clip_data).astype(float).max(axis=0)
            dark = np.array(clip_dark).astype(float).mean(axis=0)
            d = data - dark
            w_start,w_end = laser_heating.wavelength
            w = np.linspace(w_start,w_end,len(data))

            plt.figure()
            plt.plot(w,d,label='data')
            plt.plot(w,m,label='max')
            plt.title('Temperature Calibration')
            plt.xlabel('Wavelength [nm]')
            plt.ylabel('Intensity')
            #plt.xlim(600,645)
            plt.legend()
            plt.show()
            print("Save temperature calibration with name: ({calibration_name}) ([Y]/n) ? ")
            ans = click.getchar(echo=False)
            if ans.lower().startswith('n'):
                continue
            calibration_path = os.path.expanduser(laser_heating.calibration_path)
            directory_name = os.path.join(calibration_path,calibration_name)
            try:
                os.mkdir(directory_name)
            except FileExistsError:
                pass

            file_path = os.path.join(directory_name,'temp_calibration.h5')
            if os.access(file_path,os.R_OK):
                os.unlink(file_path)

            f = h5py.File(file_path, 'w')
            f['dark'] = raw_dark
            f['data'] = raw_data
            f['temperature'] = temperature
            f['epsilon'] = epsilon
            f['exposure_time'] = exposure_time
            f.close()
            break
    finally:
        camera.image.roi = previous_roi
        proxy.image_roi = previous_roi
        print("END")
        
    

def parse_args(args=None):
    parser = argparse.ArgumentParser(prog='temperature_calibration',
                                     description='Application to calibrate Laser heating with a tungsten lamp')
    parser.add_argument("--exposure-time","-e",type=float,metavar="exposure_time",help="Exposure time used for the camera")
    parser.add_argument("--temperature","-t",default=2510,metavar="temperature",help="Reference temperature")
    parser.add_argument("--epsilon",default=0.43,metavar="epsilon",help="Epsilon emissivity")
    parser.add_argument("--calibration-name",metavar="calibration_name",help="calibration name")
    return parser.parse_args(args)

