import gevent
from bliss.controllers.motor import Controller
from bliss.comm.util import get_comm
from bliss.config import settings
from bliss.common import axis as axis_module


class PZC200(Controller):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

        self.lock = gevent.lock.RLock()
        self.sock = None
        #self.accumulator = settings.HashSetting("PZC200")
        
        config = self.config.config_dict
        self.ctrl_id = config.get('controller_id',1)
        
    def initialize(self):
        self.axis_settings.config_setting["acceleration"] = False
        self.axis_settings.config_setting["velocity"] = False

        self.sock = get_comm(self.config.config_dict)

    def initialize_axis(self,axis):
        pass
    
    def initialize_hardware(self):
        with self.lock:
            self._comm(b'%dBX' % self.ctrl_id)
            gevent.sleep(0.5)
            self._comm(b'%dMO' % self.ctrl_id)
            gevent.sleep(0.5)

    def finalize(self):
        if self.sock is not None:
            self.sock.close()

    def read_position(self,axis):
        with self.lock:
            return int(self._comm(b'%dTP%d?' % (self.ctrl_id,axis.channel)))

    def set_position(self,axis,new_position):
        with self.lock:
            if new_position != 0:
                raise RuntimeError("Not manage. Can only reset the position")
            self._wait_no_motion()
            self._select_channel(axis)
            self._comm(b'%dOR' % self.ctrl_id)

    def state(self,axis):
        with self.lock:
            value = self._comm(b'%dTS?' % self.ctrl_id)
            if value == b'P':
                return axis_module.AxisState("MOVING")
            return axis_module.AxisState("READY")
            
    def start_one(self,motion):
        with self.lock:
            self._wait_no_motion()
            
            self._select_channel(motion.axis)
            self._comm(b"%dPR%d" % (self.ctrl_id,int(motion.delta)))
            #We easily overflow the communication
            #It seams that RTS/DTS doesn't work as expected
            gevent.sleep(0.05)
            self._check_error()
        
    def stop(self,axis):
        with self.lock:
            self._comm(b'%dST' % self.ctrl_id)

    def _select_channel(self,axis):
        with self.lock:
            self._comm(b'%dMX%d' % (self.ctrl_id,axis.channel))
            for i in range(8):
                if axis.channel == int(self._comm(b'%dMX?' % self.ctrl_id)):
                    break
        
    def _comm(self,message):
        reply_flag = message.find(b"?") > -1
        raw_message = b"%s\r\n" % message
        if reply_flag:
            reply_value = self.sock.write_readline(raw_message,eol=b'\r\n')
            return reply_value[len(message)+1:]
        else:
            self.sock.write(raw_message)

        
    def _check_error(self):
        with self.lock:
            error_code = int(self._comm(b"%dTE?" % self.ctrl_id))
            if error_code:
                to_human = {2:'Driver fault (thermal shut down)',
                            6:'Unknown command',
                            7:'Parameter out of range',
                            8:'No motor connected',
                            26:'Positive software limit detected',
                            27:'Negative software limit detected',
                            38:'Command parameter missing',
                            50:'Communication Overflow',
                            213:'Motor not enabled',
                            214:'Invalid axis',
                            226:'Command not allowed during motion',
                            227:'Command not allowed',
                            240:'Jog wheel over speed',
                            }
                human = to_human.get(error_code,'Unknowned Error')
                raise RuntimeError(human)
            
    
        
    def _wait_no_motion(self):
        # wait if any moving is in progress
        while axis_module.AxisState("MOVING") == self.state(None):
            gevent.sleep(0.1)
        
class Axis(axis_module.Axis):
    def __init__(self,name,controller,config):
        super().__init__(name,controller,config)

        self.channel = config["channel"]

