# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2021 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
import weakref
import struct
from gevent import lock
from bliss.comm.util import get_comm, TCP
from bliss.common.counter import SoftCounter
from bliss.common.protocols import CounterContainer
from bliss.controllers.counter import counter_namespace

_CommandType = {'A1': {'type':'i', 'unit':10**-3},  # Output Power
                'G1': {'type':'2h', 'unit':10**-3}, # Temperature limit
                'T1': {'type':'i', 'unit':10**-3},  # Temperature Sensor 
                'S1': {'type':'i', 'unit':10**-3},  # Setpoint Temperature
                'B1': {'type':'B'},                 # Mode of Operation Control Output
                }
class Module:
    class Counter:
        def __init__(self,module,name,command):
            self.module = weakref.proxy(module)
            self.name = name
            self.command = command
        @property
        def value(self):
            return self.module.command(self.command)
            
    def __init__(self,cnt,config):
        self._cnt = weakref.proxy(cnt)
        self.comm = get_comm(config,TCP)
        self._counters = list()
        self._soft_counters = list()
        for cnt_config in config.get('counters'):
            cnt = Module.Counter(self,cnt_config['cnt_name'],cnt_config['command'])
            soft_cnt = SoftCounter(cnt,'value',name=cnt.name)
            self._counters.append(cnt)
            self._soft_counters.append(soft_cnt)

    @property
    def setpoint(self):
        return self.command('S1')
    @setpoint.setter
    def setpoint(self,value):
        cmd_info = _CommandType['S1']
        value /= cmd_info.get('unit',1)
        raw_value = struct.pack('>' + cmd_info['type'],int(value))
        self.command(b's1%s\n' % raw_value)

    @property
    def heat(self):
        value = int(self.command('B1'))
        return True if value else False
    @heat.setter
    def heat(self,value):
        value = 1 if value else 0
        cmd_info = _CommandType['B1']
        raw_value = struct.pack('>' + cmd_info['type'],value)
        self.command(b'b1%s\n' % raw_value)
        
    def command(self,cmd):
        with self._cnt._lock:
            if isinstance(cmd,str):
                cmd = cmd.encode()
            self.comm.write(cmd)
            if b'A' <= cmd[0:1] <= b'Z': # Read
                while 1:
                    raw_reply = self.comm.raw_read()
                    reply = raw_reply.split(b',')[-1]
                    if reply: break
                cmd_info = _CommandType[cmd.decode()]
                size = struct.calcsize('>' + cmd_info['type'])
                raw_value = struct.unpack('>' + cmd_info['type'],reply[-size:])[0]
                return raw_value * cmd_info.get('unit',1)
            else:
                while 1:
                    raw_reply = self.comm.raw_read()
                    reply = raw_reply.split(b',')[-1]
                    if reply: break
                    
                ack_byte = struct.unpack('>B',reply)[0]
                if not ack_byte & 0x1: # Acknowledge
                    raise RuntimeError("Unknowned command has been received")
                if ack_byte & 0x2: # Not Active
                    raise RuntimeError("Command not active")
                if ack_byte & 0x4: # Outside range
                    raise RuntimeError("Command outside range")
                if ack_byte & 0x8: # Parameter changed
                    raise RuntimeError("Command processed but "
                                       "changed value of a different parameter")
                if ack_byte & 0x10: # Termination Character
                    raise RuntimeError("Command not processed, "
                                       "incorrect termination char.")
                
            
class Belektronig(CounterContainer):
    def __init__(self,name,config):
        self.name = name
        self.modules = list()
        self._lock = lock.RLock()
        for module_config in config.get('modules',list()):
            self.modules.append(Module(self,module_config))

    def __info__(self):
        info = f'Belektronig **{self.name}**\n'
        try:
            for i,module in enumerate(self.modules):
                onoff = 'ON' if int(module.command('B1')) else 'OFF'
                info += f'module {i} regulation heat {onoff}:\n'
                info += f'\tsetpoint: {module.setpoint}\n'
                temp = module.command('T1')
                temp_cnt = [x for x in module._counters if x.command == 'T1']
                if temp_cnt:
                    cnt = temp_cnt[0]
                    info += f'\ttemp: {temp} ({cnt.name})\n'
                else:
                    info += f'\ttemp: {temp}\n'
                info += '\n'
        finally:
            return info
        
    @property
    def counters(self):
        counters = list()
        for module in self.modules:
            counters.extend(module._soft_counters)
        return counter_namespace(counters)
