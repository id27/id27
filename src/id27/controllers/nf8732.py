import gevent
from bliss.controllers.motor import Controller
from bliss.comm.util import get_comm
from bliss.common import axis as axis_module
from bliss.config import settings

class NF8732(Controller):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)

        self.lock = gevent.lock.RLock()
        self.sock = None
        self.accumulator = settings.HashSetting("NF8732")
        
    def initialize(self):
        self.axis_settings.config_setting["acceleration"] = False
        self.axis_settings.config_setting["velocity"] = False

        self.sock = get_comm(self.config.config_dict)

    def initialize_axis(self,axis):
        pass
    
    def initialize_hardware(self):
        with self.lock:
            self._comm(b"*RST")
            
    def finalize(self):
        if self.sock is not None:
            self.sock.close()

    def read_position(self,axis):
        return self.accumulator.get(axis.name,0.)

    def set_position(self,axis,new_position):
        self.accumulator[axis.name] = new_position

    def state(self,axis):
        with self.lock:
            self._select_channel(axis)
            value = self._comm(b":INST:STATE?")
            if value == b'1':
                return axis_module.AxisState("MOVING")
            else:
                return axis_module.AxisState("READY")

    def start_one(self,motion):
        axis = motion.axis
        with self.lock:
            self._select_channel(axis)
            delta = motion.delta
            if delta > 0:
                self._comm(b":SOUR:DIR CW")
            else:
                self._comm(b":SOUR:DIR CCW")
            self._comm(b":SOUR:PULS:COUNT %d" % int(abs(delta)))
            acc = self.accumulator.get(axis.name,0.)
            self.accumulator[axis.name] = acc + delta

    def stop(self,axis):
        with self.lock:
            self._select_channel(axis)
            self._comm(b":INST:STATE OFF")
            
    def _select_channel(self,axis):
        self._comm(b"INST %d%d%d" % (axis.slot,axis.conn,axis.channel))

    def _comm(self,message):
        reply_flag = message.find(b"?") > -1
        raw_message = b"@%s\r" % message
        reply_value = self.sock.write_readline(raw_message,eol=b'\r')
        if reply_flag:
            return reply_value
        elif reply_value != b"OK":
            raise RuntimeError(b"message %s -> %s" % (message,reply_value))
        
class Axis(axis_module.Axis):
    def __init__(self,name,controller,config):
        super().__init__(name,controller,config)

        self.slot = config["slot"]
        self.conn = config["conn"]
        self.channel = config["channel"]

