from typing import List


class PresetManager:
    """
    - name: presets
      plugin: bliss
      package: id27.presetmanager
      class: PresetManager
      config:
        devices:
          - $diodes
          - $omega
        presets:
          look1_on:
            - diodes:
                - on: 2
                - on: 3
                - off: 4

            - omega:
                - move: 12

          look1y:
            - diodes:
                - off: 12
                - off: 14
                - off: 16

            - omega:
                - move: 4
    """

    def __init__(self, name: str, config_tree: dict):
        self.name = name
        self._config = config_tree.get("config")
        self._presets = self._config["presets"]
        self._devices = {device.name: device for device in self._config["devices"]}

        # Create methods on the object for each preset
        for preset in self._presets.keys():
            setattr(self, preset, self._make_method(preset))

    def _make_method(self, preset_name: str) -> callable:
        return lambda: self.apply(preset_name)

    def apply(self, preset_name: str) -> None:
        """Apply a preset"""
        try:
            preset = self._presets[preset_name]
        except KeyError:
            raise RuntimeError(f"No such preset `{preset_name}`")

        for device in preset:
            for device_name, settings in device.items():
                for setting in settings:
                    for func_name, args in setting.items():
                        attr = getattr(type(self._devices[device_name]), func_name)
                        # Set a property
                        if isinstance(attr, property):
                            setattr(self._devices[device_name], func_name, args)
                        # Or call a function
                        else:
                            func = getattr(self._devices[device_name], func_name)
                            if not isinstance(args, list):
                                args = [args]
                            func(*args)

    @property
    def presets(self) -> List[str]:
        """List of available presets"""
        return list(self._presets.keys())

    def __info__(self) -> str:
        info_str = "Preset Manager:\n\nDevices:\n"
        for device_name in self._devices.keys():
            info_str += f" - {device_name}\n"

        info_str += "\nAvailable Presets:\n"
        for preset_name in self._presets.keys():
            info_str += f" - {preset_name}\n"

        return info_str
