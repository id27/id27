# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2021 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
import time
from bliss.controllers.motor import Controller
from bliss.comm.util import get_comm, SERIAL, TCP
from bliss.common.axis import AxisState
from bliss.common.axis import Axis as AxisBase
from bliss.config.channels import Cache


class Axis(AxisBase):
    def activate_closed_loop(self,activate):
        active_flag = 1 if activate else 0
        self.controller.comm.write(b"SVO A %d\n" % active_flag)
        self.sync_hard()
"""
Bliss controller for E-621.CR LVPZT Controller/Amplifier Module.
"""

class PZ160(Controller):
    def __init__(self, name, config, *args, **kwargs):
        Controller.__init__(self, name, config, *args, **kwargs)
        #self.comm = get_comm(config, SERIAL,baudrate=115200,rtscts=True)
        self.comm = get_comm(config, TCP)
        self._state = AxisState()
        self._state.create_state("OVERFLOW", "voltage overflow")
        self._started_motion = Cache(self,"started_motion")
        self._last_overflowseen = None
    # Init of controller.
    def initialize(self):
        """
        Controller intialization: open a single socket for all 3 axes.
        """
        # acceleration is not mandatory in config
        self.axis_settings.config_setting["acceleration"] = False
        self.axis_settings.config_setting["velocity"] = False
        #closed loop
        self.comm.write(b"SVO A 1\n")

    def close(self):
        if self.comm is not None:
            self.comm.close()
            self.comm = None

    def initialize_axis(self, axis):
        pass

    def initialize_encoder(self, encoder):
        pass

    def read_encoder(self, encoder):
        return self._get_pos()

    def read_position(self, axis):
        return float(self.comm.write_readline(b"MOV? A\n"))

    def _get_pos(self):
        return float(self.comm.write_readline(b"POS? A\n"))

    def state(self,axis):
        state = self._state.new()
        overflow = int(self.comm.write_readline(b"OVF? A\n"))
        if overflow:
            if self._last_overflowseen is None:
                self._last_overflowseen = time.time()

            current_time = time.time()
            if current_time - self._last_overflowseen > 5:
                state.set("FAULT")
                state.set("OVERFLOW")
                state.set("READY")
                self._last_overflowseen = None
            else:
                state.set("MOVING")
        elif not self._started_motion.value:
            state.set("READY")
        else:                
            on_target = int(self.comm.write_readline(b"ONT? A\n"))
            if on_target:
                self._started_motion.value = False
                self._last_overflowseen = None
                state.set("READY")
            else:
                state.set("MOVING")
        return state
    
    def prepare_move(self, motion):
        pass

    def start_one(self, motion):
        self._started_motion.value = True
        with self.comm.lock:
            self.comm.write(b"MOV A %g\n" % motion.target_pos)
            self.comm.write_readline(b"ERR?\n")

    def stop(self,axis):
        if self._started_motion.value:
            with self.comm.lock:
                self.comm.write(b"MOV A %g\n" % self._get_pos())
                self.comm.write_readline(b"ERR?\n")

