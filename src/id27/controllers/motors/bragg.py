import numpy
from bliss.controllers import motor

class Bragg(motor.CalcController):
    DIST_SECOND_MIRROR=1190
    DIST_FIRST_MIRROR=310
    TOTAL_LENGHT = 310+1190
    
    def calc_from_real(self, positions_dict):
        y1 = positions_dict["y1"]
        y2 = positions_dict["y2"]
        offset = ((y1-y2)/self.TOTAL_LENGHT) * self.DIST_SECOND_MIRROR + y2
        angle = numpy.arctan((y1-y2)/self.TOTAL_LENGHT) * 180 / numpy.pi
        return {"offset": offset,"angle":angle}

    def calc_to_real(self, positions_dict):
        angle = positions_dict["angle"] * numpy.pi / 180.
        offset = positions_dict["offset"]
        y2 = offset - numpy.tan(angle) * self.DIST_SECOND_MIRROR
        y1 = offset + numpy.tan(angle) * self.DIST_FIRST_MIRROR
        return {"y1":y1,"y2":y2}
