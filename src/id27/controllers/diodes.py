from typing import Dict, List

from bliss.common import event
from bliss.config.channels import Cache


class Diode:
    def __init__(self, diode_id: int, wago_module, diode_name: str):
        self.diode_id = diode_id
        self.name = f"{wago_module.name}_{diode_name}"
        self._wago_module = wago_module
        self._diode_name = diode_name

        self.__state = Cache(
            self, "state", default_value="UNKNOWN", callback=self.__state_changed
        )

    @property
    def id(self) -> int:
        return self.diode_id

    @property
    def device(self) -> str:
        return self._wago_module.name

    def __state_changed(self, state):
        event.send(self, "state", state)

    def __info__(self) -> str:
        return f"{self.name}: {self.state}\n"

    @property
    def state(self) -> str:
        return "ON" if self._wago_module.get(self._diode_name) else "OFF"

    @state.setter
    def state(self, new_state: bool) -> None:
        if new_state:
            self._wago_module.set(self._diode_name, 1)
            self.__state.value = "ON"
        else:
            self._wago_module.set(self._diode_name, 0)
            self.__state.value = "OFF"


class Diodes:
    """
    plugin: bliss
    package: id27.diodes
    class: Diodes
    name: diodes
    config:
      devices:
       - $wcid27e
       - $wcid27electro
    """

    def __init__(self, name: str, config_tree: dict):
        self.name = name
        self._config = config_tree.get("config")
        self._diodes = {}

        self.__states = Cache(
            self, "states", default_value=[], callback=self.__states_changed
        )

        for wago_module in self._config["devices"]:
            for key in wago_module.modules_config.logical_keys.keys():
                if "diode" in key:
                    diode_id = key.replace("diode", "")
                    diode = Diode(diode_id, wago_module, key)
                    self._diodes[int(diode_id)] = diode

        for diode in self._diodes.values():
            event.connect(diode, "state", self._diodes_state_change)

    def _diodes_state_change(self, *args, **kwargs) -> None:
        self.__states.value = self.states

    def __states_changed(self, state) -> None:
        event.send(self, "states", state)

    @property
    def states(self) -> List[Dict[str, bool]]:
        """Return state of all child diodes"""
        return [{diode.name: diode.state} for diode in self._diodes.values()]

    @property
    def diodes(self) -> List[Diode]:
        return self._diodes

    def get(self, diode_id: int) -> Diode:
        try:
            return self._diodes[diode_id]
        except KeyError:
            raise RuntimeError(f"No such diode_id: {diode_id}")

    def state(self, diode_id: int) -> bool:
        try:
            return self._diodes[diode_id].state
        except KeyError:
            raise RuntimeError(f"No such diode_id: {diode_id}")

    def set(self, diode_id: int, state: bool):
        try:
            text_state = "ON" if state else "OFF"
            print(f"Setting diode `{diode_id}` to `{text_state}`")
            self._diodes[diode_id].state = state
        except KeyError:
            raise RuntimeError(f"No such diode_id: {diode_id}")

    def on(self, diode_id: int):
        """Turn a diode on"""
        self.set(diode_id, True)

    def off(self, diode_id: int):
        """Turn a diode off"""
        self.set(diode_id, False)

    def __info__(self) -> str:
        info_str = "Diodes:\n"
        for diode in self._diodes.values():
            info_str += f"  d{diode.id}:\t{diode.state}\t[{diode.device}]\n"

        return info_str
