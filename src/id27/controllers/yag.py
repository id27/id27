from bliss.common.soft_axis import SoftAxis as _SoftAxis

def create_yag(name,config):
    laser = config.get('laser')
    axis = _SoftAxis(name,laser,position='power',move='power', export_to_session=False)
    #to be tested (SEB)
    #axis.settings.disable_cache("_set_position")
    
    # add them into hdf5 scan start and end
    axis._positioner = True
    return axis
