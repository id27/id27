from bliss.comm import util
from bliss.common import event
from bliss.config.channels import Cache


class LaserController:
    def __init__(self,name,config):
        self.name = name
        self._cnx = util.get_comm(config,eol=b'\r')

        self.__power = Cache(
            self, "power", default_value=0, callback=self.__power_changed
        )
        self.__state = Cache(
            self, "state", default_value="UNKNOWN", callback=self.__state_changed
        )

    def __power_changed(self, power):
        """Send a signal when power changes"""
        event.send(self, "power", power)

    def __state_changed(self, state):
        """Send a signal when state changes"""
        event.send(self, "state", state)

    @property
    def state(self):
        return self.__state.value

    def on(self):
        try:
            response = self._cnx.write_readline(b"EMON\r")
            self.__state.value = "ON"
            return response
        except Exception:
            self.__state.value = "ERROR"
            raise

    def off(self):
        try:
            response = self._cnx.write_readline(b"EMOFF\r")
            self.__state.value = "OFF"
            return response
        except Exception:
            self.__state.value = "ERROR"
            raise

    @property
    def output_power(self):
        return self._cnx.write_readline(b"ROP\r")
    @property
    def power(self):
        cmd,value = self._cnx.write_readline(b"RCS \r").split(b':')
        return float(value)

    @power.setter
    def power(self,value):
        try:
            self.__power.value = value
            return self._cnx.write_readline(b"SDC %f\r" % value)
        except Exception:
            self.__state.value = "ERROR"
            raise
    
