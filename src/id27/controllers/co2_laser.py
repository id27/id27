import struct
from bliss.comm.util import get_comm

class CO2Laser:
    def __init__(self,name,config):
        self._cnx = get_comm(config)

    @property
    def pwm(self):
        sb1,sb2,pwm,power = self.info()
        return pwm
    
    @pwm.setter
    def pwm(self,percent):
        # Set PWM (or C/L SET) percentage 7Fh [Alt 0127] + hex data byte AAh
        # Note: Commanding a PWM or SET percentage of 63 (7Eh) is interpreted as a "Get
        # Status" request. Send a data byte value of 62.5% (7Dh) or 63.5% (7Fh) instead.
        if percent == 63: percent=63.5

        raw_percent = int(percent*2)
        checksum = 0xff ^ ((raw_percent + 0x7f) & 0xff)
        msg = struct.pack('BBBB',0x5b,0x7f,raw_percent,checksum)
        rx = self._cnx.write_read(msg,size=1)
        if rx[0] != 0xaa:
            raise RuntimeError("Can not set pwm percent, communication error")

    def laser_on(self):
        msg = struct.pack('BBB',0x5b,0x75,0xff^0x75)
        rx = self._cnx.write_read(msg,size=1)
        if rx[0] != 0xaa:
            raise RuntimeError("Can not enable laser, communication error")
    

    def laser_off(self):
        msg = struct.pack('BBB',0x5b,0x76,0xff^0x76)
        rx = self._cnx.write_read(msg,size=1)
        if rx[0] != 0xaa:
            raise RuntimeError("Can not set standby mode, communication error")

        
    def info(self):
        msg = struct.pack('B',0x7e)
        rx = self._cnx.write_read(msg,size=6)
        ack,sb1,sb2,pwm,power,checksum = struct.unpack('6B',rx)
        if ack != 0xaa:
            raise RuntimeError("Communication error")
        pwm /= 2.
        return sb1,sb2,pwm,power
    
    def __info__(self):
        sb1,sb2,pwm,power = self.info()
        info = ""
        MODE = {0:"MANUAL",
                1:"ANC",
                2:"ANV",
                3:"MAN CLOSED",
                4:"ANV CLOSED" ,
                5:"REMOTE"}
        info += "Operation Mode: %s\n" % MODE[sb1&0x7]
        info += "Control status: %s\n" % ("REMOTE" if sb1&(0x1<<3) else "LOCAL")
        info += "Laser: %s\n" % ("ON" if sb1&(0x1<<4) else "OFF")
        info += "Gate: %s\n" % ("pull up" if sb1&(0x1<<5) else "pull down")

        pwm_freq = 2**(sb1>>6)*5
        info += "PWM frequency: %dkHz\n" % pwm_freq

        info += "Lase on power-up: %s\n" % ("YES" if sb2&0x1 else "NO")
        info += "Maximum PWM percentage: %s\n" % ("95%" if sb2&(0x1<<1) else "99%")
        info += "Software version: %d\n" % (sb2>>4)

        info += "\n"
        info += "PWM duty cycle percentage: %f%%\n" % (pwm)
        info += "Power: %d\n" % power
        return info
