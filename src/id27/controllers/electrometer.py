from bliss.config.beacon_object import BeaconObject

class Elmeter(BeaconObject):
    def __init__(self,name,config):
        super().__init__(config,name)
        self._wago = config.get('wago')
        self._channel1 = config.get('channel1')
        self._channel2 = config.get('channel2')

    @BeaconObject.property(default=1)
    def gain1(self):
        return self._get_gain(self._channel1)
    
    @gain1.setter
    def gain1(self,val):
        return self._set_gain(self._channel1,val)

    @BeaconObject.property(default=1)
    def gain2(self):
        return self._get_gain(self._channel2)
    
    @gain2.setter
    def gain2(self,val):
        return self._set_gain(self._channel2,val)

    def _get_gain(self,channel):
        raw_gain = self._wago.get(channel)
        actived_gain = [p for p,x in zip(range(1,4),reversed(raw_gain)) if x > 0]
        if len(actived_gain) > 1:
            raise RuntimeError("Electrometer is badly set, hints: change the gain")
        elif len(actived_gain) == 0:
            raise RuntimeError("Electrometer is not initialized, no gain is defined")
        return actived_gain[0]

    
    def _set_gain(self,channel,val):
        raw_gain = [0] * 3
        if 0 < val <=3:
            raw_gain[3 - val] = 1
            self._wago.set(channel,raw_gain)
        else:
            raise ValueError("Electrometer gain is between 1 to 3")

    def __info__(self):
        return f"Electrometer \n\tgain1: {self.gain1} \n\tgain2: {self.gain2}"
