import numpy
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss.shell.standard import umv
from bliss import setup_globals

def dmesh_fscan(mot1,mot1_start,mot1_stop,mot1_nbpoints,
                mot2,mot2_start,mot2_stop,mot2_nbpoints,
                fast_mot,fast_start,fast_step_size,fast_nbpoints,integration_time):
    with cleanup(mot1, mot2, restore_list=(cleanup_axis.POS,), verbose=True):
        mot1_pos = numpy.linspace(mot1_start,mot1_stop,mot1_nbpoints)
        mot2_pos = numpy.linspace(mot2_start,mot2_stop,mot2_nbpoints)
        abs_pos1 = mot1.position
        abs_pos2 = mot2.position
        mot1_pos += abs_pos1
        mot2_pos += abs_pos2
        for m1_pos in mot1_pos:
            for m2_pos in mot2_pos:
                umv(mot1,m1_pos,
                    mot2,m2_pos)
                
                setup_globals.fscan(fast_mot,fast_start,fast_step_size,fast_nbpoints,integration_time)
