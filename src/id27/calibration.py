from bliss.config.beacon_object import BeaconObject
from bliss.scanning.scan_meta import get_user_scan_meta

class Calibration(BeaconObject):
    crysalis_conversion = BeaconObject.property_setting('crysalis_conversion',default=True)
    crysalis_update_mask = BeaconObject.property_setting('crysalis_update_mask',default=False)
    crysalis_update_par = BeaconObject.property_setting('crysalis_update_par',default=False)
    pyfai_average = BeaconObject.property_setting('pyfai_average',default=True)
    pyfai_average_output = BeaconObject.property_setting('pyfai_average_output',default='')
    pyfai_integration = BeaconObject.property_setting('pyfai_integration',default=False)
    pyfai_poni_fullpath = BeaconObject.property_setting('pyfai_poni_fullpath',default='')
    pyfai_mask_fullpath = BeaconObject.property_setting('pyfai_mask_fullpath',default='')
    pyfai_number_of_points = BeaconObject.property_setting('pyfai_number_of_points',default=1000)
    xdi_conversion = BeaconObject.property_setting('xdi_conversion',default=False)
    xds_conversion = BeaconObject.property_setting('xds_conversion',default=False)
    wavelength = BeaconObject.property_setting('wavelength',default=0.3738)
    distance = BeaconObject.property_setting('distance',default=200)
    center = BeaconObject.property_setting('center',default=(1960,1741))
    crysalis_calibration_name = BeaconObject.property_setting('crysalis_calibration_name',default='')
    crysalis_calibration_path = BeaconObject.property_setting('crysalis_calibration_path',default='')
    eiger_mask = BeaconObject.property_setting('eiger_mask',default='')
    eiger_flatfield = BeaconObject.property_setting('eiger_flatfield',default='')

    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_scan_meta = get_user_scan_meta()

        def _fill_calibration(*args):
            settings = self.settings.get_all()
            return {"calibration": {key.split('_')[-1]:str(value) for key,value in settings.items()}}
        
        user_scan_meta.instrument.set(name,_fill_calibration)

    
    def __info__(self):
        info = f"Calibration: {self.name}\n"
        info += f"\t wavelength = {self.wavelength}\n"
        info += f"\t distance = {self.distance}\n"
        info += f"\t center = {self.center}\n"
        info += f"\t eiger_mask = {self.eiger_mask}\n"
        info += f"\t eiger_flatfield = {self.eiger_flatfield}\n"
        info += f"\t crysalis_calibration_name = {self.crysalis_calibration_name}\n"
        info += f"\t crysalis_calibration_path = {self.crysalis_calibration_path}\n"
        info += f"\t crysalis_conversion = {self.crysalis_conversion}\n"
        info += f"\t crysalis_update_mask = {self.crysalis_update_mask}\n"
        info += f"\t crysalis_update_par = {self.crysalis_update_par}\n"
        info += f"\t pyfai_average = {self.pyfai_average}\n"
        info += f"\t pyfai_average_output = {self.pyfai_average_output}\n"
        info += f"\t pyfai_integration = {self.pyfai_integration}\n"
        info += f"\t pyfai_poni_fullpath = {self.pyfai_poni_fullpath}\n"
        info += f"\t pyfai_mask_fullpath = {self.pyfai_mask_fullpath}\n"
        info += f"\t pyfai_number_of_points = {self.pyfai_number_of_points}\n"
        info += f"\t xdi_conversion = {self.xdi_conversion}\n"
        info += f"\t xds_conversion = {self.xds_conversion}\n"
            
        return info
