from bliss.shell import standard
from bliss.common import session as session_mod

def move_on_click():
    session = session_mod.get_current_session()
    umv = session.env_dict['umv']
    scans = session.env_dict['SCANS']
    if not scans:
        raise RuntimeError("No scan available; Need to do a scan first!")
    last_scan = scans[-1]
    data = last_scan.get_data()

    f = standard.flint()

    scatter_plot = False
    plots = last_scan.scan_info.get('plots',[])
    if isinstance(plots,list):
        for plot_info in plots:
            kind = plot_info.get('kind')
            if kind == "scatter-plot":
                scatter_plot = True
                break

    if scatter_plot:
        p = f.get_live_plot("default-scatter")
        axis_1_name =  p.xaxis_channel_name
        axis_2_name = p.yaxis_channel_name
        
        if (not axis_1_name.startswith('axis:') or
            not axis_2_name.startswith('axis:')):
            raise RuntimeError("One of scatter axis is not a motor")
        axis_1_name = axis_1_name.split(':')[-1]
        axis_2_name = axis_2_name.split(':')[-1]
        axis1 = session.env_dict[axis_1_name]
        axis2 = session.env_dict[axis_2_name]

        position = p.select_points(1)
        axis_1_pos,axis_2_pos = position[0]
        umv(axis1,axis_1_pos,axis2,axis_2_pos)
    else:
        p = f.get_live_plot("default-curve")
        axis_name = p.xaxis_channel_name
        if axis_name.find('axis:') == -1:
            raise RuntimeError("Can't find an axis on plot")

        axis_name = axis_name.split(':')[-1]
        axis = session.env_dict[axis_name]

        position = p.select_points(1)
        motor_pos = position[0][0]
        umv(axis,motor_pos)
