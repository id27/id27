from matplotlib import pyplot as plt

def plot_wien(lambdas,wien_spectrum,wien_fit):
    plt.figure()
    plt.plot(1/lambdas,wien_spectrum,label='Wien data')
    plt.plot(1/lambdas,wien_fit,'--k')
    plt.title('Wien fit')
    plt.xlabel('1/ Wavelength [m]')
    plt.ylabel('Wien [a.u.]')
    plt.legend()
    plt.show()
