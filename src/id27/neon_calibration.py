import argparse
import click
import numpy as np
from matplotlib import pyplot as plt
import lmfit
from bliss.config import static
from bliss.common.scans.ct import ct
from bliss.common import session

NEON_LIST=np.array([
    (5852.48794,0.85),
    (5881.89524,0.083),
    (5944.83424,0.152),
    (5975.53404,0.058),
    (6029.99694,0.055),
    (6074.33774,0.154),
    (6096.16314,0.228),
    (6143.06264,0.427),
    (6163.59394,0.185),
    (6217.28124,0.1),
    (6266.49504,0.29),
    (6304.78894,0.083),
    (6334.42784,0.211),
    (6382.99174,0.53),
    (6402.248,0.67),
    (6506.52814,0.4),
    (6532.88224,0.19),
    (6598.95294,0.2),
    (6678.27624,0.43),
    (6717.04304,0.26),
    (6929.46734,0.3),
    (7024.05044,0.4),
    (7032.41314,0.9),
    (7173.93814,0.06),
    (7245.16664,0.5),
    (7438.9,0.15),
    (7488.87124,0.03),
    (7492.102,0.03),
    (7535.77414,0.08),
    (8377.6080,0.22),
    (8495.3598,0.1),
    (8780.6226,0.1)])


def gaussian(x,peak,width):
    return 1/(width*np.sqrt(2*np.pi))*np.exp(-0.5*((x-peak)/width)**2)

def wavelength(pixels, offset, slope):
    return slope*np.arange(len(pixels)) + offset

def model(pixels, offset, slope, peaks, width, strength, scale = 1):
    wavelength = slope*np.arange(len(pixels)) + offset
    intens = np.zeros(len(wavelength))
    for i in range(len(peaks)):
        intens += scale*strength[i]*gaussian(wavelength,peaks[i],width)
    return intens

cfg = static.get_config()
def main(args=None):
    laser_heating = cfg.get('laser_heating')
    return _main(args,laser_heating=laser_heating)

def main_lab(args=None):
    laser_heating = cfg.get('laser_heating_lab')
    return _main(args,laser_heating=laser_heating)

def _main(args=None,laser_heating=None):
    default_session = session.DefaultSession()
    session_env = dict()
    default_session.setup(session_env)

    arguments = parse_args(args)
    exposure_time = arguments.exposure_time
    camera = laser_heating.camera


    #take the dark
    camera.shutter.mode = "MANUAL"
    s = ct(exposure_time,camera.image)
    raw_dark = s.get_data()['image'].get_image(0)
    #take data
    camera.shutter.mode = "AUTO_FRAME"
    s = ct(exposure_time,camera.image)
    raw_data = s.get_data()['image'].get_image(0)

    while True:
        data = np.array(raw_data).astype(float).max(axis=0)
        dark = np.array(raw_dark).astype(float).max(axis=0)
        neon = np.array(NEON_LIST).astype(float)[:,0]/10
        neon_intens = np.array(NEON_LIST).astype(float)[:,1]
        d = data-dark


        # estimate offset and slope from 2 strongest peaks
        max1 = np.argmax(d)
        l1 = neon[np.argmax(neon_intens)]
        tmp = np.copy(d)
        for i in range(20):
            tmp[max1+10-i]=0
        max2 = np.argmax(tmp)
        tmp = np.copy(neon_intens)
        tmp[np.argmax(neon_intens)]=0
        l2 = neon[np.argmax(tmp)]
        sl = (l2-l1)/(max2-max1)
        off = l1 -sl*max1
        #print('Estimation from two strongest peaks : slope = ', sl, 'offset = ', off )


        # fit all peaks and plot
        neon_model = lmfit.Model(model,independent_vars=['pixels', 'peaks', 'strength'])
        params = lmfit.Parameters()
        params.add_many(('offset',  off , True, 700, 1300, ),
                        ('slope', sl , True, -0.5, -0.1, ),
                        ('width',  0.5 , True, 0, 10, ),
                        ('scale', 1.5, True, 0.5, 2))

        result = neon_model.fit(d/max(d), params, pixels=d,peaks=neon, strength=neon_intens )
        print(result.fit_report())

        popt = []
        perr = []
        for param in result.params.values():
            popt.append(param.value)
            perr.append(param.stderr)

        sl = popt[1]
        off =  popt[0]
        width = popt[2]
        scale = popt[3]
        try:
            print('Fit resulst: slope = ', np.round(popt[1],4), '(', np.round(perr[1],4), ') offset = ', np.round(popt[0],4), '(', np.round(perr[0],4),')')
        except TypeError:
            print('Fitting not sucesssful. Please adjust noen intensity')
            
        w =  wavelength(d, off, sl)
        intens = model(d, off, sl, neon, width, neon_intens,scale)

        plt.figure()
        plt.plot(w,d,label='data')
        if popt[0] is not None:
            plt.plot(w,intens*max(d),'--k', label= 'fit: ' + 'slope = ' + str(np.round(popt[1],4)) + ' offset = ' + str(np.round(popt[0],2)))
        else:
            print('Fitting not sucesssful. Please adjust noen intensity') 
        plt.title('Neon Calibration')
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Intensity')
        plt.xlim(600,645)
        plt.legend()
        plt.show()

        print("Save neon calibration ([Y]/n) ? ")
        ans = click.getchar(echo=False)
        if ans.lower().startswith('n'):
            continue

        w_start = w[0]
        w_end = w[-1]
        laser_heating.wavelength = w_start,w_end
        break

def parse_args(args=None):
    parser = argparse.ArgumentParser(prog='neon_calibration',
                                     description='Application to calibrate Laser heating with a neon lamp')
    parser.add_argument("--exposure-time","-e",type=float,metavar="exposure_time",help="Exposure time used for the camera")
    return parser.parse_args(args)



if __name__ == "__main__":
    main()


