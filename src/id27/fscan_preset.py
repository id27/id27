import os
import json
from bliss.scanning.scan import ScanPreset
from tango import DeviceProxy

_proxy = DeviceProxy('DAU/dahu/1')

class FScanMuxPreset(ScanPreset):
    def __init__(self, calib):
        super().__init__()
        self.limadevs = list()
        self.mcadevs = list()
        self._calib = calib

    def set_fscan_master(self, master):
        self.limadevs = master.lima_used
        self.mcadevs = master.mca_used

    def start(self, scan):
        pass

    def stop(self,scan):
        scan_name = scan.scan_info.get('type')
        scan_info = scan.scan_info['instrument']['fscan_parameters']

        calib_settings = self._calib.settings.get_all()
        wave_length=calib_settings['wavelength']
        distance=calib_settings['distance']
        center=calib_settings['center']
        calibration_name = self._calib.crysalis_calibration_name
        calibration_path = self._calib.crysalis_calibration_path
        crysalis_update_mask = self._calib.crysalis_update_mask
        crysalis_update_par = self._calib.crysalis_update_par

        dirname = os.path.dirname(scan.writer.filename)
        scan_number='scan%.4d'% (scan._Scan__scan_number)
        if scan_name == 'fscan':
            start_pos=scan_info['start_pos']
            step_size=scan_info['step_size']
            exposure_time=scan_info['acq_time']
            number_of_points=scan_info['npoints']
            file_source_path=os.path.join(dirname,scan_number,'eiger_0000.h5')
            try:
                conversion_params = {"file_path" : dirname,
                                     "scan_number" : scan_number,
                                     }
                if calib_settings.get('crysalis_conversion',False):
                    json_params = json.dumps({"wave_length" : wave_length,
                                              "distance" : distance,
                                              "center" : center,
                                              "omega_start" : start_pos,
                                              "omega_step" : step_size,
                                              "exposure_time" : exposure_time,
                                              "number_of_points" : number_of_points,
                                              "file_source_path" : file_source_path,
                                              "scan_name" : scan_number,
                                              "calibration_path" : calibration_path,
                                              "calibration_name" : calibration_name,
                                              "update_mask" : crysalis_update_mask,
                                              "update_par" : crysalis_update_par,
                                          })
                    
                    _proxy.startJob(["id27.crysalisconversion",json_params])
                if calib_settings.get('xdi_conversion',False):
                    _proxy.startJob(["id27.xdiconversion", json.dumps(conversion_params)])

                if calib_settings.get('pyfai_average',False):
                    if "pyfai_average_output" in dir(self._calib):
                        pyfai_average_output = self._calib.pyfai_average_output
                        conversion_params["output_directory"] = pyfai_average_output
                    _proxy.startJob(["id27.average", json.dumps(conversion_params)])
                    
                if calib_settings.get('xds_conversion',False):
                    _proxy.startJob(["id27.xdsconversion", json.dumps(conversion_params)])

            except:
                print("Warning! Can not do file conversion\n"
                      "Start DahuDs on lid27crystalbar")
                raise
                
        elif scan_name == 'fscan2d' or scan_name == 'fscan3d':
            start_pos=scan_info['fast_start_pos']
            step_size=scan_info['fast_step_size']
            exposure_time=scan_info['acq_time']
            npoints=scan_info['fast_npoints']
            motor_mode = scan_info['fast_motor_mode']
            conversion_params = {"file_path" : dirname,
                                 "scan_number" : scan_number,
                                 }
            try:
                if calib_settings.get('crysalis_conversion',False):
                    json_params = json.dumps({"wave_length" : wave_length,
                                              "distance" : distance,
                                              "center" : center,
                                              "omega_start" : start_pos,
                                              "omega_step" : step_size,
                                              "exposure_time" : exposure_time,
                                              "npoints" : npoints,
                                              "motor_mode" : motor_mode,
                                              "dirname" : os.path.join(dirname,scan_number),
                                              "scan_name" : scan_number,
                                              "calibration_path" : calibration_path,
                                              "calibration_name" : calibration_name,
                                          })

                    _proxy.startJob(["id27.crysalisconversionfscannd",json_params])

                if calib_settings.get('xdi_conversion',False):
                    _proxy.startJob(["id27.xdiconversion", json.dumps(conversion_params)])

                if calib_settings.get('pyfai_average',False):
                    if "pyfai_average_output" in dir(self._calib):
                        pyfai_average_output = self._calib.pyfai_average_output
                        conversion_params["output_directory"] = pyfai_average_output
                    _proxy.startJob(["id27.average", json.dumps(conversion_params)])
                    
                if calib_settings.get('xds_conversion',False):
                    _proxy.startJob(["id27.xdsconversion", json.dumps(conversion_params)])

            except :
                print("Warning! Can not do file conversion\n"
                      "Start DahuDs on lid27crystalbar")
                raise
                
            
