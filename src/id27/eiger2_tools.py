import json
import numpy
import requests
import argparse


from base64 import b64encode, b64decode


EIGER_HOST = "lid27eiger2dcu.esrf.fr"
mask_url = f'http://{EIGER_HOST}/detector/api/1.8.0/config/pixel_mask'
flatfield_url = f'http://{EIGER_HOST}/detector/api/1.8.0/config/flatfield'

def get_mask():
    
    reply = requests.get(mask_url)
    reply.raise_for_status()
    darray = reply.json()['value']

    return numpy.frombuffer(b64decode(darray['data']),
                            dtype=numpy.dtype(str(darray['type']))).reshape(darray['shape'])



def set_mask(ndarray):
    data_json = json.dumps({'value':{
                            '__darray__': (1,0,0),
                            'type': ndarray.dtype.str,
                            'shape': ndarray.shape,
                            'filters': ['base64'],
                            'data': b64encode(ndarray.data).decode('ascii')}
                            })
    headers = {'Content-Type': 'application/json'}
    reply = requests.put(mask_url, data=data_json, headers=headers)
    reply.raise_for_status()


def get_flatfield():
    reply = requests.get(flatfield_url)
    reply.raise_for_status()
    
    darray = reply.json()['value']

    return numpy.frombuffer(b64decode(darray['data']),
                            dtype=numpy.dtype(str(darray['type']))).reshape(darray['shape'])

    

def set_flatfield(ndarray):
    data_json = json.dumps({'value':{
                            '__darray__': (1,0,0),
                            'type': ndarray.dtype.str,
                            'shape': ndarray.shape,
                            'filters': ['base64'],
                            'data': b64encode(ndarray.data).decode('ascii')}
                            })
    headers = {'Content-Type': 'application/json'}
    reply = requests.put(flatfield_url, data=data_json, headers=headers)
    reply.raise_for_status()

def load_eiger_flatfield(filename):
    flatfield = numpy.load(filename)
    set_flatfield(flatfield)
    
def load_eiger_mask(filename):
    mask = numpy.load(filename)
    set_mask(mask)

    
def main(args=None):
    arguments = parse_args(args)
    if arguments.write:
        if arguments.mask:
            load_eiger_mask(arguments.mask)
        if arguments.flatfield:
            load_eiger_flatfield(arguments.flatfield)
    else:                       # READ
        if arguments.mask:
            mask = get_mask()
            with open(arguments.mask,'wb+') as f:
                numpy.save(f,mask)
        if arguments.flatfield:
            flatfield = get_flatfield()
            with open(arguments.flatfield,'wb+') as f:
                numpy.save(f,flatfield)
            

def parse_args(args=None):
    parser = argparse.ArgumentParser(prog='eiger_tools',
                                     description='Eiger tools to manage mask and flatfield')
    parser.add_argument("--mask",type=str,metavar="mask_filename",help="mask filename")
    parser.add_argument("--flatfield",type=str,metavar="flatfield_filename",help="flatfile filename")
    parser.add_argument("--write",dest='write',default=False,action='store_true')
    return parser.parse_args(args)

