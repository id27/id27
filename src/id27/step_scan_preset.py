import os
import json
from xmlrpc import client
from bliss.scanning.chain import ChainPreset
from .fscan_preset import _proxy

class ScanConversion(ChainPreset):
    def __init__(self,calib,eiger):
        super().__init__()
        self._calib = calib
        self._eiger = eiger

    def stop(self,chain):
        scan = chain.scan
        scan_info = scan.scan_info
        calib_settings = self._calib.settings.get_all()
        scan_name = scan_info.get('type')
        eiger_active = len(scan.acq_chain.get_node_from_devices(self._eiger))
        dirname = os.path.dirname(scan.writer.filename)
        scan_number='scan%.4d'% (scan._Scan__scan_number)

        calib_settings = self._calib.settings.get_all()
        wave_length=calib_settings['wavelength']
        distance=calib_settings['distance']
        center=calib_settings['center']
        calibration_name = self._calib.crysalis_calibration_name
        calibration_path = self._calib.crysalis_calibration_path

        number_of_points=int(scan_info['npoints'])

        if eiger_active:
            if scan_name == 'dmesh':
                start_pos = 0
                step_size = 0
                exposure_time=scan_info['count_time']
                file_source_path=os.path.join(dirname,scan_number,'eiger_0000.h5')
                try:
                    conversion_params = {"file_path" : dirname,
                                                         "scan_number" : scan_number,
                                                     }
                    if calib_settings.get('crysalis_conversion',False):
                        json_params = json.dumps({"wave_length" : wave_length,
                                                  "distance" : distance,
                                                  "center" : center,
                                                  "omega_start" : start_pos,
                                                  "omega_step" : step_size,
                                                  "exposure_time" : exposure_time,
                                                  "number_of_points" : number_of_points,
                                                  "file_source_path" : file_source_path,
                                                  "scan_name" : scan_number,
                                                  "calibration_path" : calibration_path,
                                                  "calibration_name" : calibration_name,
                        })
                        _proxy.startJob(["id27.crysalisconversion",json_params])
                    if calib_settings.get('xdi_conversion',False):
                        _proxy.startJob(["id27.xdiconversion",json.dumps(conversion_params)])

                    if calib_settings.get('pyfai_average',False):
                        if "pyfai_average_output" in dir(self._calib):
                             pyfai_average_output = self._calib.pyfai_average_output
                             conversion_params["output_directory"] = pyfai_average_output
                        _proxy.startJob(["id27.average", json.dumps(conversion_params)])

                    if calib_settings.get('xds_conversion',False):
                        _proxy.startJob(["id27.xdsconversion",json.dumps(conversion_params)])

                    if calib_settings.get('pyfai_integration',False):
                        pyfai_poni_fullpath = calib_settings.get('pyfai_poni_fullpath')
                        pyfai_mask_fullpath = calib_settings.get('pyfai_mask_fullpath')
                        pyfai_number_of_points = calib_settings.get('pyfai_number_of_points')
                        if not pyfai_poni_fullpath or not pyfai_mask_fullpath:
                            print("Poni or mask file not defined, cancel intergration")
                            return
                        
                        fast_scan = scan_info.get('npoints1')
                        slow_scan = scan_info.get('npoints2')
                        json_params = json.dumps({"ponifile":pyfai_poni_fullpath,
                                                  "maskfile":pyfai_mask_fullpath,
                                                  "npt": pyfai_number_of_points,
                                                  "file_path": dirname,
                                                  "scan_number": scan_number,
                                                  "fast_scan" : fast_scan,
                                                  "slow_scan" : slow_scan})
                        _proxy.startJob(["id27.diffmap",json_params])
                except:
                    print("Warning! Can not do file conversion\n"
                          "Start DahuDs on lid27crystalbar")
                    raise

            if scan_name == 'ct' and scan_info.get('save',False):
                try:
                    conversion_params = {"file_path" : dirname,
                                                         "scan_number" : scan_number,
                                                     }
                    if calib_settings.get('pyfai_average',False):
                        if "pyfai_average_output" in dir(self._calib):
                            pyfai_average_output = self._calib.pyfai_average_output
                            conversion_params["output_directory"] = pyfai_average_output
                        _proxy.startJob(["id27.average", json.dumps(conversion_params)])

                    if calib_settings.get('pyfai_integration',False):
                        pyfai_poni_fullpath = calib_settings.get('pyfai_poni_fullpath')
                        pyfai_mask_fullpath = calib_settings.get('pyfai_mask_fullpath')
                        pyfai_number_of_points = calib_settings.get('pyfai_number_of_points')
                        if not pyfai_poni_fullpath or not pyfai_mask_fullpath:
                            print("Poni or mask file not defined, cancel intergration")
                            return
                        
                        json_params = json.dumps({"ponifile":pyfai_poni_fullpath,
                                                  "maskfile":pyfai_mask_fullpath,
                                                  "npt": pyfai_number_of_points,
                                                  "file_path": dirname,
                                                  "scan_number": scan_number,
                                                  "fast_scan" : 1,
                                                  "slow_scan" : 1})
                        _proxy.startJob(["id27.diffmap",json_params])

                except:
                    print("Warning! Can not do file conversion\n"
                          "Start DahuDs on lid27crystalbar")
                    raise
