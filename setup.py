# -*- coding: utf-8 -*-

"""The setup script."""

import sys
from setuptools import setup, find_packages

TESTING = any(x in sys.argv for x in ['test', 'pytest'])

with open('README.rst') as readme_file:
    readme = readme_file.read()

requirements = ['bliss']

setup_requirements = ['pytest-runner', 'pytest'] if TESTING else []

test_requirements = ['pytest-cov', 'mock']
console_script_entry_points = ["neon_calibration = id27.neon_calibration:main",
                               "neon_calibration_lab = id27.neon_calibration:main_lab",
                               "temperature_calibration = id27.temperature_calibration:main",
                               "temperature_calibration_lab = id27.temperature_calibration:main_lab",
                               "eiger_tools = id27.eiger2_tools:main"]
    
setup(
    author="BCU Team",
    author_email='sebastien.petitdemange@esrf.fr',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    description="ID27 bliss software",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme,
    include_package_data=True,
    keywords='id27',
    name='id27',
    entry_points={"console_scripts": console_script_entry_points},
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.esrf.fr/ID27/id27',
    version='0.1.0',
    zip_safe=False,
)
